<?php

namespace App\Http\Controllers;
use Bootstrap\app;
use Illuminate\Http\Request;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Puntuacion;

class PuntuacioneController extends BaseController
{
    public function index() {
        return Puntuacion::where('deleted_at', null)->get();
    }

    public function createComment(Request $request) {
        $data = $request->all();
        $comentario = new Puntuacion();
        $comentario->id_tienda = $data['id_tienda'];
        $comentario->id_usuario = $data['id_usuario'];
        $comentario->id_compra = $data['id_compra'];
        $comentario->puntuacion = $data['puntuacion'];
        $comentario->comentario = $data['comentario'];
        $new = $comentario->save();
        return response()->json(['msg' => 'Guardado']);
    }

    public function updateComment($id, Request $request) {
        $data = $request->all();
        $comentario = Puntuacion::where('id_compra', $id)
                                ->update(['comentario' => $data['comentario']]);
        return response()->json(['msg' => 'Editado']);
    }

    public function deleteComment($id) {
        $comentario = Puntuacion::where('id', $id)->update(['deleted_at' => date('Y-m-d H:i:s')]);
        return response()->json(['msg' => 'Elimiando']);
    }
    
    public function searchComment($id){
        return Puntuacion::where('id', $id)->firstOrFail();
    }

    public function searchCommentCompra($id){
        return Puntuacion::where('id_compra', $id)->get();
    }

    public function searchCommentUser(Request $request){
        $data = $request->all();
        $start = ($data['fecha_start']) ? $data['fecha_start'] : null;
        $end = ($data['fecha_end']) ? $data['fecha_end'] : date('Y-m-d H:i:s');
        if ($start == null) {
            $user = Puntuacion::where('id_usuario', $data['id'])->get();
        }
        else {
            $user = Puntuacion::where('id_usuario', $data['id'])
                        ->whereBetween('created_at', [$start, $end])
                        ->get();
        }
        return $user;       
    }

    public function searchCommentTienda(Request $request){
        $data = $request->all();
        $start = ($data['fecha_start']) ? $data['fecha_start'] : null;
        $end = ($data['fecha_end']) ? $data['fecha_end'] : date('Y-m-d H:i:s');
        if ($start == null) {
            $user = Puntuacion::where('id_tienda', $data['id'])->get();
        }
        else {
            $user = Puntuacion::where('id_tienda', $data['id'])
                        ->whereBetween('created_at', [$start, $end])
                        ->get();
        }
        return $user;       
    }

    public function puntuacion($tienda){
        $puntuacion = Puntuacion::selectRaw('avg(puntuacion) as promedio')
                                ->where('id_tienda', $tienda)
                                ->where('deleted_at', null)
                                ->firstOrFail();
        return $puntuacion;
    }

    public function sendSqs(){
        $send = dispatch(new SendJob);
        return Queue::push(new SendJob);
    }
}
