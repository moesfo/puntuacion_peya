<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Puntuacione extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_tienda', 'id_usuario', 'id_compra', 'puntuacion', 'comentario', 'created_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at'
    ];
}
