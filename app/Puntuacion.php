<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Puntuacion extends Model 
{
    public $table = "puntuaciones";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}