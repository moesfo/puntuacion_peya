<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Puntuacione::class, function (Faker\Generator $faker) {
    return [
        'id_tienda' => $faker->numberBetween($min = 1, $max = 50),
        'id_usuario' => $faker->numberBetween($min = 1, $max = 50),
        'id_compra' => $faker->numberBetween($min = 1, $max = 50),
        'puntuacion' => $faker->numberBetween($min = 1, $max = 5),
        'comentario' => $faker->lexify('Comentario ???'),
        'created_at' => $faker->dateTimeInInterval($startDate = '-1 years', $interval = '+ 390 days', $timezone = null),
    ];
});
