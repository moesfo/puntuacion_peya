<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Puntuacione;

class PuntuacioneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Puntuacione::class, 40)->create()->each(function ($Puntuacione) {
            $Puntuacione->save([factory(App\Models\Puntuacione::class)->make()]);
        });
    }
}
