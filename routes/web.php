<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/', function () {
    return 'Puntuaciones peya, puede iniciar con /puntuaciones';
});


$router->get('puntuaciones', 'PuntuacioneController@index');

$router->get('puntuaciones/{id}', 'PuntuacioneController@searchComment');

$router->post('guardar', 'PuntuacioneController@createComment');

$router->put('editar/{id}', 'PuntuacioneController@updateComment');

$router->get('eliminar/{id}', 'PuntuacioneController@deleteComment');

$router->get('buscar_compra/{id}', 'PuntuacioneController@searchCommentCompra');

$router->get('buscar_usuario[/{id}, /{fecha_start}, /{fecha_end}]', 'PuntuacioneController@searchCommentUser');

$router->get('buscar_tienda[/{ida}, /{fecha_start}, /{fecha_end}]', 'PuntuacioneController@searchCommentTienda');

$router->get('puntuacion_tienda/{id}', 'PuntuacioneController@puntuacion');